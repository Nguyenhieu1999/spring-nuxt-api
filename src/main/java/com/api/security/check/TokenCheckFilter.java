package com.api.security.check;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSON;
import com.api.common.base.Result;
import com.api.common.constant.Base;
import com.api.common.constant.ResultCode;
import com.api.common.redis.RedisUtil;
import com.api.config.AppProperties;
import com.api.security.JwtFactory;
import com.api.security.check.extractor.Extractor;
import com.api.security.check.extractor.OldTokenValueExtractor;

public class TokenCheckFilter extends AccessControlFilter {

    private static final Logger LOG = LoggerFactory.getLogger(TokenCheckFilter.class);

    private AppProperties appProperties;

    private RedisUtil redisUtil;

    private JwtFactory jwtFactory;

    private Extractor extractor;

    public TokenCheckFilter(AppProperties appProperties, JwtFactory jwtFactory, RedisUtil redisUtil) {
        this(appProperties, jwtFactory, redisUtil, new OldTokenValueExtractor());
    }

    public TokenCheckFilter(AppProperties appProperties, JwtFactory jwtFactory, RedisUtil redisUtil, Extractor extractor) {
        this.appProperties = appProperties;
        this.jwtFactory = jwtFactory;
        this.redisUtil = redisUtil;
        this.extractor = extractor;
    }



    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        String token = getHeaderToken(request);


        if(null == token || "".equals(token) || "undefined".equals(token)) {
            write2Response(response, Result.error(ResultCode.USER_NOT_LOGGED_IN));
            return true;
        }

        JwtToken authToken = createToken(request, response);
        boolean tokenExpired = false;
        try {
            Subject subject = getSubject(request, response);
            subject.login(authToken);
        } catch (UnknownAccountException e) {
            LOG.error(e.getMessage(),e);
            write2Response(response, Result.error(ResultCode.USER_NOT_EXIST));
            return false;
        }catch (ExpiredCredentialsException e) {
            LOG.error(e.getMessage(),e);
            tokenExpired = true;
        } catch (CredentialsException e) {
            LOG.error(e.getMessage(),e);
            write2Response(response, Result.error(ResultCode.INVALID_TOKEN));
            return false;
        }

        boolean sessionNotTimeout = redisUtil.hasKey(token);
        if(!sessionNotTimeout) {
            write2Response(response, Result.error(ResultCode.SESSION_TIMEOUT));
            return false;
        }

        if(tokenExpired) {
            Object oldTokenValue = redisUtil.get(token);
            if(null == oldTokenValue || extractor.supports(oldTokenValue.toString())) {

            } else {
                String username = oldTokenValue.toString();
                String refreshToken = jwtFactory.generateToken(username);
                redisUtil.set(refreshToken, username, appProperties.getSecurity().getTimeout());
                redisUtil.set(token, extractor.generate(username), appProperties.getSecurity().getOldTokenAliveTime());

                HttpServletResponse httpResponse = (HttpServletResponse)response;
                httpResponse.setHeader(Base.TOKEN_NAME, refreshToken);
            }

        } else {
            redisUtil.expire(token, appProperties.getSecurity().getTimeout());
        }


        return true;

    }

    private void write2Response(ServletResponse response, Result result) throws Exception{
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        JSON.writeJSONString(response.getWriter(), result);
    }

  
    protected JwtToken createToken(ServletRequest request, ServletResponse response) {
        String jwtToken = getHeaderToken(request);
        return new JwtToken(jwtToken);
    }

    protected String getHeaderToken(ServletRequest request) {
        return ((HttpServletRequest)request).getHeader(appProperties.getSecurity().getJwt().getHeader());

    }

}
