package com.api.security.check.extractor;


public interface Extractor {

    String extract(String payload);

    String generate(String data);

    boolean supports(String payload);

}
