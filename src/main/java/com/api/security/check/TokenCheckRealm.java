package com.api.security.check;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.api.common.constant.Base;
import com.api.common.redis.RedisUtil;
import com.api.entity.User;
import com.api.security.JwtFactory;
import com.api.security.check.extractor.Extractor;
import com.api.security.check.extractor.OldTokenValueExtractor;
import com.api.service.UserService;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class TokenCheckRealm extends AuthorizingRealm {

    private UserService userService;
    private JwtFactory jwtFactory;
    private RedisUtil redisUtil;
    private Extractor extractor;

    public TokenCheckRealm(UserService userService, JwtFactory jwtFactory, RedisUtil redisUtil, Extractor extractor) {
        this.userService = userService;
        this.jwtFactory = jwtFactory;
        this.redisUtil = redisUtil;
        this.extractor = extractor;
    }

    public TokenCheckRealm(UserService userService, JwtFactory jwtFactory, RedisUtil redisUtil) {
        this(userService, jwtFactory, redisUtil, new OldTokenValueExtractor());
    }
    public Class<?> getAuthenticationTokenClass() {
        return JwtToken.class;
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String jwtToken = (String) token.getPrincipal();

        String username = null;
        try {
            username = jwtFactory.getUsernameFromToken(jwtToken);
        }catch (ExpiredJwtException e) {
            Object tokenValue = redisUtil.get(jwtToken);
            if(null != tokenValue) {
                username = extractor.extract(tokenValue.toString());
            }
            throw new ExpiredCredentialsException("Token expired", e);
        }catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException e) {
            throw new CredentialsException("Token is invalid", e);
        }

        User user = userService.getUserByAccount(username);

        if (user == null) {
            throw new UnknownAccountException();
        }

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user,
                true,
                getName()
        );

        return authenticationInfo;
    }


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        User user = (User)principals.getPrimaryPrincipal();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Set<String> roles = new HashSet<String>();

        if (user.getAdmin()) {
            roles.add(Base.ADMIN_ROLE_CODE);
        } else {
            roles.add(Base.COMMON_ROLE_CODE);
        }

        info.setRoles(roles);

        return info;

    }
}