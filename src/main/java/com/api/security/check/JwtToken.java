package com.api.security.check;

import org.apache.shiro.authc.AuthenticationToken;


public class JwtToken implements AuthenticationToken {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6927167138274442389L;
	private String jwtToken;


    public JwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }


    @Override
    public Object getPrincipal() {
        return this.jwtToken;
    }

    @Override
    public Object getCredentials() {
        return true;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
