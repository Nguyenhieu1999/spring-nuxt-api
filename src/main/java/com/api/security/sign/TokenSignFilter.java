package com.api.security.sign;

import com.alibaba.fastjson.JSON;
import com.api.common.base.Result;
import com.api.common.constant.ResultCode;

import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class TokenSignFilter extends AccessControlFilter {

    private static final Logger LOG = LoggerFactory.getLogger(TokenSignFilter.class);

    private static final String DEFAULT_USERNAME_PARAM = "account";
    private static final String DEFAULT_PASSWORD_PARAM = "password";



    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

        if(null != getSubject(request, response) && getSubject(request, response).isAuthenticated()) {
            return true;
        }

        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        AuthenticationToken token = createToken(request, response);
        Result r = new Result();
        try {
            Subject subject = getSubject(request, response);
            subject.login(token);//认证
            return true;
        } catch (UnknownAccountException e) {
            LOG.error(e.getMessage(),e);
            r.setResultCode(ResultCode.USER_NOT_EXIST);
        }catch (AuthenticationException e) {
            LOG.error(e.getMessage(),e);
            r.setResultCode(ResultCode.USER_LOGIN_ERROR);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            r.setResultCode(ResultCode.ERROR);
        }

        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        JSON.writeJSONString(response.getWriter(), r);

        return false;

    }


    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {

        String username = ((HttpServletRequest)request).getParameter(DEFAULT_USERNAME_PARAM);
        String password= ((HttpServletRequest)request).getParameter(DEFAULT_PASSWORD_PARAM);
        return new UsernamePasswordToken(username, password);
    }
}
