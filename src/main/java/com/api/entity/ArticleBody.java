package com.api.entity;

import org.hibernate.annotations.Type;

import com.api.common.base.entity.BaseEntity;

import javax.persistence.*;


@Entity
@Table(name = "me_article_body")
public class ArticleBody extends BaseEntity<Long> {

    /**
     *
     */
    private static final long serialVersionUID = -7611409995977927628L;


    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Type(type = "text")
    private String content; 

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Type(type = "text")
    private String contentHtml;


	private long salt;


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public String getContentHtml() {
        return contentHtml;
    }


    public void setContentHtml(String contentHtml) {
        this.contentHtml = contentHtml;
    }
    public void setSalt(long salt) {
        this.salt=salt;
   }


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public long getSalt() {
		return salt;
	}

}
