package com.api.entity;

import org.hibernate.validator.constraints.NotBlank;

import com.api.common.base.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "me_category")
public class Category extends BaseEntity<Integer> {

    /**
     *
     */
    private static final long serialVersionUID = 5025313969040054182L;

    private String categoryname;

    private String description;

    private String avatar;

	private long salt;


    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public void setSalt(long salt) {
        this.salt=salt;
   }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getSalt() {
		return salt;
	}

}
