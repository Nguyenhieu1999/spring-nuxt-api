package com.api.entity;

import org.hibernate.validator.constraints.NotBlank;

import com.api.common.base.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "me_tag")
public class Tag extends BaseEntity<Integer> {

    /**
     *
     */
    private static final long serialVersionUID = 5025313969040054182L;

    private String tagname;

    private String avatar;

	private long salt;


    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public void setSalt(long salt) {
        this.salt=salt;
   }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getSalt() {
		return salt;
	}


}
