package com.api.entity;


public enum UserStatus {

    normal("normal"), blocked("blocked");

    private final String info;
	private long salt;

    private UserStatus(String info) {
        this.info = info;
    }

    public long getSalt() {
		return salt;
	}

	public String getInfo() {
        return info;
    }
    public void setSalt(long salt) {
        this.salt=salt;
   }
}
