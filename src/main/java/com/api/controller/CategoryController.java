package com.api.controller;

import com.api.common.annotation.LogAnnotation;
import com.api.common.base.Result;
import com.api.common.constant.Base;
import com.api.common.constant.ResultCode;
import com.api.entity.Category;
import com.api.service.CategoryService;
import com.api.vo.CategoryVO;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {


    @Autowired
    private CategoryService categoryService;

    @GetMapping
    @LogAnnotation(module = "Category", operation = "Get all article categories")
    public Result listCategorys() {
        List<Category> category = categoryService.findAll();

        return Result.success(category);
    }

    @GetMapping("detail")
    @LogAnnotation(module = "Category", operation = "Get all article categories, detailed")
    public Result listCategorysDetail() {
        List<CategoryVO> category = categoryService.findAllDetail();

        return Result.success(category);
    }

    @GetMapping("/{id}")
    @LogAnnotation(module = "Category", operation = "Get article classification according to id")
    public Result getCategoryById(@PathVariable("id") Integer id) {

        Result r = new Result();

        if (null == id) {
            r.setResultCode(ResultCode.PARAM_IS_BLANK);
            return r;
        }

        Category category = categoryService.getCategoryById(id);

        r.setResultCode(ResultCode.SUCCESS);
        r.setData(category);
        return r;
    }

    @GetMapping("/detail/{id}")
    @LogAnnotation(module = "Category", operation = "Get detailed article classification and number of articles by id")
    public Result getCategoryDetail(@PathVariable("id") Integer id) {

        Result r = new Result();

        if (null == id) {
            r.setResultCode(ResultCode.PARAM_IS_BLANK);
            return r;
        }

        CategoryVO category = categoryService.getCategoryDetail(id);

        r.setResultCode(ResultCode.SUCCESS);
        r.setData(category);
        return r;
    }

    @PostMapping("/create")
    @RequiresRoles(Base.ADMIN_ROLE_CODE)
    @LogAnnotation(module = "Category", operation = "Add article categories")
    public Result saveCategory(@Validated @RequestBody Category category) {

        Integer categoryId = categoryService.saveCategory(category);

        Result r = Result.success();
        r.simple().put("categoryId", categoryId);
        return r;
    }

    @PostMapping("/update")
    @RequiresRoles(Base.ADMIN_ROLE_CODE)
    @LogAnnotation(module = "Category", operation = "Modify article categories")
    public Result updateCategory(@RequestBody Category category) {
        Result r = new Result();

        if (null == category.getId()) {
            r.setResultCode(ResultCode.USER_NOT_EXIST);
            return r;
        }

        Integer categoryId = categoryService.updateCategory(category);

        r.setResultCode(ResultCode.SUCCESS);
        r.simple().put("categoryId", categoryId);
        return r;
    }

    @GetMapping("/delete/{id}")
    @RequiresRoles(Base.ADMIN_ROLE_CODE)
    @LogAnnotation(module = "Category", operation = "Delete article categories")
    public Result deleteCategoryById(@PathVariable("id") Integer id) {
        Result r = new Result();

        if (null == id) {
            r.setResultCode(ResultCode.PARAM_IS_BLANK);
            return r;
        }

        categoryService.deleteCategoryById(id);

        r.setResultCode(ResultCode.SUCCESS);
        return r;
    }


}
