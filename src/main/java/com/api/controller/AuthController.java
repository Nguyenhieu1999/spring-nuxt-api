package com.api.controller;

import com.api.common.base.Result;
import com.api.common.constant.Base;
import com.api.common.constant.ResultCode;
import com.api.common.redis.RedisUtil;
import com.api.config.AppProperties;
import com.api.entity.User;
import com.api.security.JwtFactory;
import com.api.service.UserService;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/auth")
@RestController
public class AuthController {

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtFactory jwtFactory;

    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/login")
    public Result login(User user) {

        User userDetail = userService.getUserByAccount(user.getAccount());

        String token = jwtFactory.generateToken(user);
        redisUtil.set(token, user.getAccount(), appProperties.getSecurity().getTimeout());

        Result r = Result.success();
        r.simple().put(Base.TOKEN_NAME, token);

        return r;
    }

    @GetMapping("/logout")
    public Result logout(HttpServletRequest request) {
        Result r = new Result();
        String token = request.getHeader(appProperties.getSecurity().getJwt().getHeader());
        redisUtil.del(token);

        Subject subject = SecurityUtils.getSubject();
        subject.logout();

        r.setResultCode(ResultCode.SUCCESS);
        return r;
    }


}
