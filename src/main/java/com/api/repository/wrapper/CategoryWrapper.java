package com.api.repository.wrapper;

import java.util.List;

import com.api.vo.CategoryVO;

public interface CategoryWrapper {

    List<CategoryVO> findAllDetail();

    CategoryVO getCategoryDetail(Integer categoryId);
}
