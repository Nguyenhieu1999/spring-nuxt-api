package com.api.repository.wrapper;

import java.util.List;

import com.api.vo.TagVO;


public interface TagWrapper {

    List<TagVO> findAllDetail();

    TagVO getTagDetail(Integer tagId);
}
