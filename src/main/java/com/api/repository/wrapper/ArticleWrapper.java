package com.api.repository.wrapper;

import com.api.entity.Article;
import com.api.vo.ArticleVo;
import com.api.vo.PageVo;

import java.util.List;

public interface ArticleWrapper {
    List<Article> listArticles(PageVo page);

    List<Article> listArticles(ArticleVo article, PageVo page);

    List<ArticleVo> listArchives();

}
