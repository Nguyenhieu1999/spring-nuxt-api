package com.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.entity.Article;
import com.api.entity.Comment;

import java.util.List;


public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findByArticleAndLevelOrderByCreateDateDesc(Article a, String level);


}
