package com.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.entity.Log;

public interface LogRepository extends JpaRepository<Log, Integer> {
}
