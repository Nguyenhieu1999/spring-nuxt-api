package com.api.common.controlleradvice;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.api.common.base.Result;
import com.api.common.constant.ResultCode;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler(AuthorizationException.class)
    @ResponseStatus(HttpStatus.OK)
    public Result AuthorizationExceptionHandler(HttpServletRequest request, AuthorizationException e) {

    	logger.error ("Lỗi xác thực quyền, uri: {}, do:", request.getRequestURI (), e);

        return Result.error(ResultCode.NO_ACCESS_PERMISSION);
    }

    @ExceptionHandler(UnauthenticatedException.class)
    @ResponseStatus(HttpStatus.OK)
    public Result UnauthenticatedExceptionHandler(HttpServletRequest request, UnauthenticatedException e) {

    	logger.error ("Lỗi xác thực đăng nhập, uri: {}, do:", request.getRequestURI (), e);

        return Result.error(ResultCode.USER_NOT_LOGGED_IN);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    public Result ExceptionHandler(HttpServletRequest request, Exception e) {

    	logger.error ("Lỗi hệ thống, uri: {}, do:", request.getRequestURI (), e);

        return Result.error(ResultCode.SYSTEM_ERROR);
    }


}
