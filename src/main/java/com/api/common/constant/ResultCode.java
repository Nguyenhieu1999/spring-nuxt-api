package com.api.common.constant;


public enum ResultCode {

    SUCCESS(Base.SUCCESS_CODE, "Thành công"),
    ERROR(Base.ERROR_CODE, "Lỗi"),
    PARAM_IS_INVALID(10001, "Đối số không hợp lệ"),
    PARAM_IS_BLANK (10002, "Tham số trống"),
    PARAM_TYPE_BIND_ERROR (10003, "Lỗi loại thông số"),
    PARAM_NOT_COMPLETE (10004, "Thiếu tham số"),
    USER_NOT_LOGGED_IN (20001, "Người dùng chưa đăng nhập"),
    USER_LOGIN_ERROR (20002, "Lỗi tài khoản hoặc mật khẩu"),
    USER_ACCOUNT_FORBIDDEN (20003, "Tài khoản đã bị vô hiệu hóa"),
    USER_NOT_EXIST (20004, "Người dùng không tồn tại"),
    USER_HAS_EXISTED (20005, "Người dùng đã tồn tại"),
    USER_REGISTER_ERROR (20006, "Lỗi đăng ký người dùng"),
    PASSWORD_ERROR (20006, "Mật khẩu ban đầu không đúng"),
    SYSTEM_ERROR (30001, "Dịch vụ bất thường"),
    NO_ACCESS_PERMISSION (70001, "Không có quyền truy cập"),
    INVALID_TOKEN (70002, "Mã thông báo không hợp lệ"),
    SESSION_TIMEOUT (70003, "Thời gian chờ của phiên"),
    UPLOAD_ERROR (80001, "Tải lên không thành công");
    private Integer code;

    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public static String getMessage(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.message;
            }
        }
        return name;
    }

    public static Integer getCode(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.code;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

}
