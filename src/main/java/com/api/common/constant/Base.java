package com.api.common.constant;


public class Base {

    public static final int SUCCESS_CODE = 0;

    public static final int ERROR_CODE = -1;

    public static final int DEFAULT_S_NUMBER = 99;

    public static final int SINGLE_PAGE_ITEM_TYPE = 2;

    public static final String TOKEN_NAME= "Access-Token";

    public static final String COMMON_ROLE_CODE = "common";

    public static final String ADMIN_ROLE_CODE = "admin";

    public static final String PERMISSION_USER_REGISTER = "user:register";

    public static final String CURRENT_USER = "current_user";

}
