package com.api.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DateUtil {


    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";


    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";


    public static final String DEFAULT_MONTH = "MONTH";


    public static final String DEFAULT_YEAR = "YEAR";


    public static final String DEFAULT_DATE = "DAY";


    public static final String DEFAULT_HOUR = "HOUR";


    public static final String DEFAULT_MINUTE = "MINUTE";


    public static final String DEFAULT_SECOND = "SECOND";


    public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH-mm";


    public static final String DEFAULT_DATETIME_FORMAT_SEC = "yyyy-MM-dd HH:mm:ss";


    public static final String[] WEEKS = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thusday",
            "Friday", "Saturday" };

    public static String today() {
        return today(DEFAULT_DATE_FORMAT);
    }

    public static String today(String strFormat) {
        return toString(new Date(), strFormat);
    }

    public static String currentTime() {
        return currentTime(DEFAULT_TIME_FORMAT);
    }


    public static String currentTime(String strFormat) {
        return toString(new Date(), strFormat);
    }


    public static String getAddDay(String field, int amount)
            throws ParseException {
        return getAddDay(field, amount, null);
    }

    public static String getAddDay(String field, int amount, String strFormat)
            throws ParseException {
        return getAddDay(null, field, amount, strFormat);
    }


    public static String getAddDay(String date, String field, int amount,
                                   String strFormat) throws ParseException {
        if (strFormat == null) {
            strFormat = DEFAULT_DATETIME_FORMAT_SEC;
        }
        Calendar rightNow = Calendar.getInstance();
        if (date != null && !"".equals(date.trim())) {
            rightNow.setTime(parseDate(date, strFormat));
        }
        if (field == null) {
            return toString(rightNow.getTime(), strFormat);
        }
        rightNow.add(getInterval(field), amount);
        return toString(rightNow.getTime(), strFormat);
    }

   
    protected static int getInterval(String field) {
        String tmpField = field.toUpperCase();
        if (tmpField.equals(DEFAULT_YEAR)) {
            return Calendar.YEAR;
        } else if (tmpField.equals(DEFAULT_MONTH)) {
            return Calendar.MONTH;
        } else if (tmpField.equals(DEFAULT_DATE)) {
            return Calendar.DATE;
        } else if (DEFAULT_HOUR.equals(tmpField)) {
            return Calendar.HOUR;
        } else if (DEFAULT_MINUTE.equals(tmpField)) {
            return Calendar.MINUTE;
        } else {
            return Calendar.SECOND;
        }
    }

    public static SimpleDateFormat getSimpleDateFormat(String strFormat) {
        if (strFormat != null && !"".equals(strFormat.trim())) {
            return new SimpleDateFormat(strFormat);
        } else {
            return new SimpleDateFormat();
        }
    }

    public static String getWeekOfMonth() throws ParseException {
        return getWeekOfMonth(null, null);
    }

   
    public static String getWeekOfMonth(String date, String fromat)
            throws ParseException {
        Calendar rightNow = Calendar.getInstance();
        if (date != null && !"".equals(date.trim())) {
            rightNow.setTime(parseDate(date, fromat));
        }
        return WEEKS[rightNow.get(Calendar.WEEK_OF_MONTH)];
    }


    public static String toString(Date date, String format) {
        return getSimpleDateFormat(format).format(date);
    }

   
    public static String toString(Date date) {
        return toString(date, DEFAULT_DATE_FORMAT);
    }

    public static Date parseDate(String strDate, String format)
            throws ParseException {
        return getSimpleDateFormat(format).parse(strDate);
    }


    public static String millisecondFormat(Long millisecond, String format) {
        if (millisecond == null || millisecond <= 0) {
            throw new IllegalArgumentException(String.format("The incoming time milliseconds [%s] is illegal",
                    "" + millisecond));
        }
        if (format == null || "".equals(format.trim())) {
            format = DEFAULT_DATE_FORMAT;
        }
        return toString(new Date(millisecond), format);
    }

    public static Timestamp parseTimestamp(String strDate, String format)
            throws ParseException {
        Date utildate = getSimpleDateFormat(format).parse(strDate);
        return new Timestamp(utildate.getTime());
    }

    public static Date getCurDate() {
        return (new Date());
    }

    public static Timestamp getCurTimestamp() {
        return new Timestamp(new Date().getTime());
    }


    public static Date getCurDate(String format) throws Exception {
        return getSimpleDateFormat(format).parse(toString(new Date(), format));
    }

    
    public static String toString(Timestamp timestamp, String format) {
        if (timestamp == null) {
            return "";
        }
        return toString(new Date(timestamp.getTime()), format);
    }

    public static String toString(Timestamp ts) {
        return toString(ts, DEFAULT_DATE_FORMAT);
    }

    public static String toString(Timestamp timestamp, boolean fullFormat) {
        if (fullFormat) {
            return toString(timestamp, DEFAULT_DATETIME_FORMAT_SEC);
        } else {
            return toString(timestamp, DEFAULT_DATE_FORMAT);
        }
    }

   
    public static String toString(java.sql.Date sqldate, String sFormat) {
        if (sqldate == null) {
            return "";
        }
        return toString(new Date(sqldate.getTime()), sFormat);
    }

    public static String toString(java.sql.Date sqldate) {
        return toString(sqldate, DEFAULT_DATE_FORMAT);
    }

    public static Map<String, Long> timeDifference(final Date date1,
                                                   final Date date2) {
        if (date1 == null || date2 == null) {
            throw new NullPointerException("date1 and date2 can't null");
        }
        long mim1 = date1.getTime();
        long mim2 = date2.getTime();
        if (mim1 < mim2) {
            throw new IllegalArgumentException(String.format(
                    "date1[%s] not be less than date2[%s].", mim1 + "", mim2
                            + ""));
        }
        long m = (mim1 - mim2 + 1) / 1000l;
        long mday = 24 * 3600;
        final Map<String, Long> map = new HashMap<String, Long>();
        map.put("day", m / mday);
        m = m % mday;
        map.put("hour", (m) / 3600);
        map.put("minute", (m % 3600) / 60);
        map.put("second", (m % 3600 % 60));
        return map;
    }

    public static Map<String, Integer> compareTo(final Date date1,
                                                 final Date date2) {
        if (date1 == null || date2 == null) {
            return null;
        }
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        long time = Math.max(time1, time2) - Math.min(time1, time2);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("year", (calendar.get(Calendar.YEAR) - 1970) > 0 ? (calendar
                .get(Calendar.YEAR) - 1970) : 0);
        map.put("month", (calendar.get(Calendar.MONTH) - 1) > 0 ? (calendar
                .get(Calendar.MONTH) - 1) : 0);
        map.put("day",
                (calendar.get(Calendar.DAY_OF_MONTH) - 1) > 0 ? (calendar
                        .get(Calendar.DAY_OF_MONTH) - 1) : 0);
        map.put("hour",
                (calendar.get(Calendar.HOUR_OF_DAY) - 8) > 0 ? (calendar
                        .get(Calendar.HOUR_OF_DAY) - 8) : 0);
        map.put("minute", calendar.get(Calendar.MINUTE) > 0 ? calendar
                .get(Calendar.MINUTE) : 0);
        map.put("second", calendar.get(Calendar.SECOND) > 0 ? calendar
                .get(Calendar.SECOND) : 0);
        return map;
    }

    public static void main(String[] args) {
        System.out.println(today());
    }
}

