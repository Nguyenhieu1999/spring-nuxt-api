package com.api.config;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.api.common.redis.RedisUtil;
import com.api.security.CustomizedModularRealmAuthenticator;
import com.api.security.JwtFactory;
import com.api.security.check.TokenCheckFilter;
import com.api.security.check.TokenCheckRealm;
import com.api.security.sign.TokenSignFilter;
import com.api.security.sign.TokenSignRealm;
import com.api.service.UserService;

@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager, TokenCheckFilter tokenCheckFilter) {

        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("sign", new TokenSignFilter());
        filterMap.put("check", tokenCheckFilter);
        shiroFilterFactoryBean.setFilters(filterMap);


        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();

        filterChainDefinitionMap.put("/api/**", "anon");

        filterChainDefinitionMap.put("/auth/login", "sign");

        filterChainDefinitionMap.put("/**", "check");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        return shiroFilterFactoryBean;
    }

    @Bean
    public TokenCheckFilter tokenCheckFilter(AppProperties appProperties, JwtFactory jwtFactory, RedisUtil redisUtil) {
        return new TokenCheckFilter(appProperties, jwtFactory, redisUtil);
    }


    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        hashedCredentialsMatcher.setHashIterations(2);
        return hashedCredentialsMatcher;
    }

    @Bean
    public TokenSignRealm tokenSignRealm(UserService userService, HashedCredentialsMatcher credentialsMatcher) {
        TokenSignRealm tokenSignRealm = new TokenSignRealm(userService);
        tokenSignRealm.setCredentialsMatcher(credentialsMatcher);
        return tokenSignRealm;
    }

    @Bean
    public TokenCheckRealm tokenCheckRealm(UserService userService, JwtFactory jwtFactory, RedisUtil redisUtil) {
        TokenCheckRealm tokenCheckRealm = new TokenCheckRealm(userService, jwtFactory, redisUtil);
        return tokenCheckRealm;
    }

    @Bean
    public SecurityManager securityManager(List<Realm> realms) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setAuthenticator(new CustomizedModularRealmAuthenticator());

        securityManager.setRealms(realms);

        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        securityManager.setSubjectDAO(subjectDAO);

        return securityManager;
    }


    @Bean
    public static LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}
