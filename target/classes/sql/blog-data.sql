

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
BEGIN;
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
INSERT INTO `hibernate_sequence` VALUES (13);
COMMIT;

-- ----------------------------
-- Table structure for me_article
-- ----------------------------
DROP TABLE IF EXISTS `me_article`;
CREATE TABLE `me_article` (
  `id` int(11) NOT NULL,
  `comment_counts` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `summary` varchar(100) COLLATE utf8_bin NOT NULL,
  `title` varchar(40) COLLATE utf8_bin NOT NULL,
  `view_counts` int(11) DEFAULT NULL,
  `weight` int(11) NOT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `body_id` bigint(20) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `picture` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKndx2m69302cso79y66yxiju4h` (`author_id`),
  KEY `FKrd11pjsmueckfrh9gs7bc6374` (`body_id`),
  KEY `FKjrn3ua4xmiulp8raj7m9d2xk6` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of me_article
-- ----------------------------
BEGIN;
INSERT INTO `me_article` VALUES (1, 0, '2019-03-01 14:31:40', '111111111111111111\n\n二级标题\n', '11111111111', 7, 0, 1, 2, 1, NULL);
INSERT INTO `me_article` VALUES (5, 0, '2019-03-01 14:36:06', '\n222222222222222\n', '2222222', 7, 0, 1, 6, 1, 'http://localhost:8888/20190301/9247c516-0060-4364-9dee-8445da619de3_KW3kWikn5cI.jpg');
INSERT INTO `me_article` VALUES (7, 0, '2019-03-01 14:42:34', '33333333\n', '3333333', 3, 0, 1, 8, 1, 'http://localhost:8888/20190301/9f920651-4157-4116-8a6a-17bc6f886f61_KW3kWikn5cI.jpg');
INSERT INTO `me_article` VALUES (9, 0, '2019-01-01 14:44:57', '444444444444\n', '4444444444', 13, 0, 1, 10, 1, 'http://localhost:8888/20190301/fca4a04c-b7fc-4893-abe5-1a272d7b47dd_KW3kWikn5cI.jpg');
INSERT INTO `me_article` VALUES (11, 0, '2019-02-01 15:13:56', '在前端项目开发过程中，总是会引入一些UI框架，已为方便自己的使用，很多大公司都有自己的一套UI框架，下面就是最近经常使用并且很流行的UI框架...', '目前流行前端几大UI框架', 19, 0, 1, 12, 2, '');
COMMIT;

-- ----------------------------
-- Table structure for me_article_body
-- ----------------------------
DROP TABLE IF EXISTS `me_article_body`;
CREATE TABLE `me_article_body` (
  `id` bigint(20) NOT NULL,
  `content` longtext COLLATE utf8_bin,
  `contentHtml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of me_article_body
-- ----------------------------
BEGIN;
INSERT INTO `me_article_body` VALUES (2, '111111111111111111\n\n![KW3kWikn5cI.jpg](http://localhost:8888/20190301/01696613-1a79-4ea5-af35-fae07c7f4204_KW3kWikn5cI.jpg)\n\n\n\n## 二级标题', '<p>111111111111111111</p>\n<p><img src=\"http://localhost:8888/20190301/01696613-1a79-4ea5-af35-fae07c7f4204_KW3kWikn5cI.jpg\" alt=\"KW3kWikn5cI.jpg\" /></p>\n<h2><a id=\"_6\"></a>二级标题</h2>\n');
INSERT INTO `me_article_body` VALUES (4, '![KW3kWikn5cI.jpg](http://localhost:8888/20190301/9247c516-0060-4364-9dee-8445da619de3_KW3kWikn5cI.jpg)\n\n\n222222222222222', '<p><img src=\"http://localhost:8888/20190301/9247c516-0060-4364-9dee-8445da619de3_KW3kWikn5cI.jpg\" alt=\"KW3kWikn5cI.jpg\" /></p>\n<p>222222222222222</p>\n');
INSERT INTO `me_article_body` VALUES (6, '![KW3kWikn5cI.jpg](http://localhost:8888/20190301/9247c516-0060-4364-9dee-8445da619de3_KW3kWikn5cI.jpg)\n\n\n222222222222222', '<p><img src=\"http://localhost:8888/20190301/9247c516-0060-4364-9dee-8445da619de3_KW3kWikn5cI.jpg\" alt=\"KW3kWikn5cI.jpg\" /></p>\n<p>222222222222222</p>\n');
INSERT INTO `me_article_body` VALUES (8, '33333333![KW3kWikn5cI.jpg](http://localhost:8888/20190301/9f920651-4157-4116-8a6a-17bc6f886f61_KW3kWikn5cI.jpg)', '<p>33333333<img src=\"http://localhost:8888/20190301/9f920651-4157-4116-8a6a-17bc6f886f61_KW3kWikn5cI.jpg\" alt=\"KW3kWikn5cI.jpg\" /></p>\n');
INSERT INTO `me_article_body` VALUES (10, '444444444444![KW3kWikn5cI.jpg](http://localhost:8888/20190301/fca4a04c-b7fc-4893-abe5-1a272d7b47dd_KW3kWikn5cI.jpg)', '<p>444444444444<img src=\"http://localhost:8888/20190301/fca4a04c-b7fc-4893-abe5-1a272d7b47dd_KW3kWikn5cI.jpg\" alt=\"KW3kWikn5cI.jpg\" /></p>\n');
INSERT INTO `me_article_body` VALUES (12, '在前端项目开发过程中，总是会引入一些UI框架，已为方便自己的使用，很多大公司都有自己的一套UI框架，下面就是最近经常使用并且很流行的UI框架。\n\n#### Mint UI\n\n![屏幕快照 2019-01-18 下午3.03.59.png](https://upload-images.jianshu.io/upload_images/4770884-33175b386f93f986.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/545/format/webp)\n\nMint UI是 饿了么团队开发基于vue .js的移动端UI框架，它包含丰富的 CSS 和 JS 组件，能够满足日常的移动端开发需要。\n\n官网：https://mint-ui.github.io/#!/zh-cn\nGithub: https://github.com/ElemeFE/mint-ui/\n\n', '<p>在前端项目开发过程中，总是会引入一些UI框架，已为方便自己的使用，很多大公司都有自己的一套UI框架，下面就是最近经常使用并且很流行的UI框架。</p>\n<h4><a id=\"Mint_UI_2\"></a>Mint UI</h4>\n<p><img src=\"https://upload-images.jianshu.io/upload_images/4770884-33175b386f93f986.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/545/format/webp\" alt=\"屏幕快照 2019-01-18 下午3.03.59.png\" /></p>\n<p>Mint UI是 饿了么团队开发基于vue .js的移动端UI框架，它包含丰富的 CSS 和 JS 组件，能够满足日常的移动端开发需要。</p>\n<p>官网：https://mint-ui.github.io/#!/zh-cn<br />\nGithub: https://github.com/ElemeFE/mint-ui/</p>\n');
COMMIT;

-- ----------------------------
-- Table structure for me_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `me_article_tag`;
CREATE TABLE `me_article_tag` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `FK2s65pu9coxh7w16s8jycih79w` (`tag_id`),
  KEY `FKsmysra6pt3ehcvts18q2h4409` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of me_article_tag
-- ----------------------------
BEGIN;
INSERT INTO `me_article_tag` VALUES (1, 1);
INSERT INTO `me_article_tag` VALUES (5, 1);
INSERT INTO `me_article_tag` VALUES (7, 1);
INSERT INTO `me_article_tag` VALUES (9, 1);
INSERT INTO `me_article_tag` VALUES (11, 2);
COMMIT;

-- ----------------------------
-- Table structure for me_category
-- ----------------------------
DROP TABLE IF EXISTS `me_category`;
CREATE TABLE `me_category` (
  `id` int(11) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL,
  `categoryname` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of me_category
-- ----------------------------
BEGIN;
INSERT INTO `me_category` VALUES (1, 'http://localhost:8888/category/front.png', '前端', NULL);
INSERT INTO `me_category` VALUES (2, 'http://localhost:8888/category/back.png', '后端', NULL);
INSERT INTO `me_category` VALUES (3, 'http://localhost:8888/category/lift.jpg', '生活', NULL);
INSERT INTO `me_category` VALUES (4, 'http://localhost:8888/category/database.png', '数据库', NULL);
INSERT INTO `me_category` VALUES (5, 'http://localhost:8888/category/language.png', '编程语言', NULL);
COMMIT;

-- ----------------------------
-- Table structure for me_comment
-- ----------------------------
DROP TABLE IF EXISTS `me_comment`;
CREATE TABLE `me_comment` (
  `id` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8_bin NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `level` varchar(1) COLLATE utf8_bin DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `to_uid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKecq0fuo9k0lnmea6r01vfhiok` (`article_id`),
  KEY `FKkvuyh6ih7dt1rfqhwsjomsa6i` (`author_id`),
  KEY `FKaecafrcorkhyyp1luffinsfqs` (`parent_id`),
  KEY `FK73dgr23lbs3ebex5qvqyku308` (`to_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for me_tag
-- ----------------------------
DROP TABLE IF EXISTS `me_tag`;
CREATE TABLE `me_tag` (
  `id` int(11) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL,
  `tagname` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of me_tag
-- ----------------------------
BEGIN;
INSERT INTO `me_tag` VALUES (1, '/tag/java.png', 'Java');
INSERT INTO `me_tag` VALUES (2, '/tag/', 'Spring');
INSERT INTO `me_tag` VALUES (3, '/tag/hibernate.svg', 'Hibernate');
INSERT INTO `me_tag` VALUES (4, '/tag/maven.png', 'Maven');
INSERT INTO `me_tag` VALUES (5, '/tag/html.png', 'Html');
INSERT INTO `me_tag` VALUES (6, '/tag/js.png', 'JavaScript');
INSERT INTO `me_tag` VALUES (7, '/tag/vue.png', 'Vue');
INSERT INTO `me_tag` VALUES (8, '/tag/css.png', 'Css');
COMMIT;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `method` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `module` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `operation` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `params` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL,
  `account` varchar(10) COLLATE utf8_bin NOT NULL,
  `admin` bit(1) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `email` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `mobile_phone_number` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(10) COLLATE utf8_bin NOT NULL,
  `password` varchar(64) COLLATE utf8_bin NOT NULL,
  `salt` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_awpog86ljqwb89aqa1c5gvdrd` (`account`),
  UNIQUE KEY `UK_ahtq5ew3v0kt1n7hf1sgp7p8l` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 'shimh', b'1', '/avatar2.jpg', '2018-01-22 17:14:49', b'0', '919431514@qq.com', NULL, '18396816462', '史明辉', 'c237910910ffa1f4827bf7fe1831ce43', 'e4153a582cbc45c3a199998b506dab28', 'normal');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
